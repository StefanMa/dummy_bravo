import time
from lara_simulation.simulated_serial import SimulatedDevice
from threading import Thread


SUCCESS = 0
BAD_ARGS = 1
FAIL = 2


class VirtualBravo(SimulatedDevice):
    def __init__(self, slots):
        super(VirtualBravo, self).__init__()
        self.initialized = False
        self.busy = False
        self.logged_in = False
        self.messages = []
        self.last_protocol = "unknown"
        self.slots = slots

    def RunProtocol(self, protocol: str, count: int = 1):
        self.last_protocol = protocol
        self.do_stuff(5*count, "ProtocolComplete")
        return SUCCESS

    def Login(self, user: str, pw: str):
        self.logged_in = True
        return SUCCESS

    def AbortProtocol(self):
        return SUCCESS

    def Logout(self):
        self.logged_in = False
        return SUCCESS

    def ReinitializeDevices(self):
        self.do_stuff(4, 'InitializationComplete')
        self.initialized = True
        return SUCCESS

    def GetMessages(self):
        if len(self.messages) == 0:
            return ""
        else:
            return self.messages.pop()

    def do_stuff(self, seconds, message: str):
        def stuff(_seconds, _message):
            self.busy = True
            if self.fast_mode:
                _seconds = _seconds/20
            time.sleep(_seconds)
            self.messages.append(f"{_message}|simulation")
            self.busy = False
        t = Thread(target=stuff, args=[seconds, message], daemon=True)
        t.start()

    def __str__(self):
        user = f"user: {None if not self.logged_in else 'admin'}"
        initialized = f"initialized: {self.initialized}"
        action = f"currently: {'busy' if self.busy else 'idle'}"
        messages = "messages:" + '\n          '.join(self.messages[-2:])
        slots = [f'{i}|XXXX|' if self.slots[i] else '|____|' for i in self.slots]
        slots = '  '.join(slots[:4])
        s = '\n'.join([user, initialized, action, messages, slots])
        return s
