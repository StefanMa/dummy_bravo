from lara_utility.wrapper import construct_VWorks_wrapper
from lara_utility.inner_status import InnerStatus
import logging
from typing import Union, List
import time
import os
import re
from threading import Thread
from enum import Enum
from bravo_server.virtual_bravo import VirtualBravo


class RetCode(Enum):
    SUCCESS = 0
    BAD_ARGS = 1
    FAIL = 2


class BravoProtocol:
    def __init__(self, simulation: bool):
        self.simulation = simulation
        prot_dir = os.path.dirname(os.path.realpath(__file__)) if simulation else r"C:\VWorks Workspace\Protocol Files"
        self.inner_state = InnerStatus(name="Bravo", simulation_mode=simulation,
                                       defaults=dict(
                                           protocol_dir=prot_dir,
                                           device_file=r"C:\VWorks Workspace\Device Files\Bravo96LT no Gripper.dev",
                                           occupied=[True, True, True,
                                                     False, False, False,
                                                     False, False, False, ]
                                       ))
        self.wrapper = VirtualBravo(self.inner_state.occupied) if simulation else construct_VWorks_wrapper()
        self._stop = False
        self.messages = []
        self.initialized = False
        self.running_protocol = False
        t = Thread(target=self.receive_messages, daemon=True)
        t.start()
        self.transfer_mode = False
        self.login()
        time.sleep(1)
        self.initialize()

    def receive_messages(self):
        while not self._stop:
            message = self.wrapper.GetMessages()
            if not message == '':
                print(message)
                self.messages.append(message)
                parts = message.split('|')
                if parts[0] == "ProtocolComplete":
                    self.running_protocol = False
                if parts[0] == "InitializationComplete":
                    self.initialized = True
            time.sleep(1)

    # utility functions
    def finish_protocol(self, timeout=60) -> bool:
        patience_end = time.time() + timeout
        while self.running_protocol:
            time.sleep(1)
            if time.time() > patience_end:
                print("Can not wait any longer for protocol to finish")
                self.abort_protocol()
                return False
        print("all protocols finished")
        return True

    def get_head_out_of_way(self, site: int) -> bool:
        if site in [4, 5, 7, 8]:
            protocol = "Head_Left.pro"
        elif site in [3, 6]:
            protocol = "Head_Right.pro"
        else:
            return False
        print(f"getting my head out of the way to site{site}... running {protocol}")
        answer = self.run_protocol(protocol, 1, ignore_transfer_mode=True)
        if not answer == RetCode.SUCCESS:
            print(f"Failed to run safety-of-head protocol. answer:{answer}")
            return False
        return self.finish_protocol(120)

    def run_protocol(self, protocol: str, run_count: int = 1, ignore_transfer_mode=False) -> RetCode:
        if self.transfer_mode and not ignore_transfer_mode:
            logging.error("Can not run protocols while in transfer mode")
            return RetCode.FAIL
        path = os.path.join(self.inner_state.protocol_dir, protocol)
        i = self.wrapper.RunProtocol(path, run_count)
        if i == RetCode.SUCCESS.value:
            self.running_protocol = True
        return RetCode(i)

    def abort_protocol(self) -> RetCode:
        if self.running_protocol:
            self.running_protocol = False
        self.wrapper.AbortProtocol()

    def close_device_file(self, file: str) -> RetCode:
        pass

    def close_protocol(self, file: str) -> RetCode:
        pass

    def compile_protocol(self, file: str) -> RetCode:
        pass

    def get_simulation_mode(self) -> bool:
        pass

    def get_tip_state(self, protocol: str) -> RetCode:
        pass

    def login(self, user: str = 'admin', pw: str = 'Thermo') -> RetCode:
        i = self.wrapper.Login(user, pw)
        return RetCode(i)

    def logout(self):
        i = self.wrapper.Logout()
        return RetCode(i)

    def open_device_file(self) -> RetCode:
        i = self.wrapper.LoadDeviceFile(self.inner_state.device_file)
        return RetCode(i)

    def initialize(self) -> RetCode:
        i = self.wrapper.ReinitializeDevices()
        return RetCode(i)

    def available_protocols(self) -> List[str]:
        content = os.listdir(self.inner_state.protocol_dir)
        protocols = [file for file in content if file.endswith('.pro')]
        return protocols

    def stop(self):
        self.logout()
        self._stop = True

    def __str__(self):
        if self.simulation:
            return str(self.wrapper)
        else:
            return "Hello World! This is the AgilentBravo protocol"
