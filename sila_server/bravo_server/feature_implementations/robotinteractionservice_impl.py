from __future__ import annotations

from typing import Any, Dict, List
import time
from datetime import timedelta
from sila2.framework import FullyQualifiedIdentifier, CommandExecutionStatus
from sila2.server import ObservableCommandInstance

import bravo_server.bravo_protocol as bp


from ..generated.robotinteractionservice import (
    ContainerGotTaken_Responses,
    PrepareToGiveContainerFromSite_Responses,
    PrepareToReceiveContainerOnSite_Responses,
    ReceivedContainer_Responses,
    RobotInteractionServiceBase,
    TransportCancelled_Responses,
)


class RobotInteractionServiceImpl(RobotInteractionServiceBase):
    def __init__(self, parent_server, hardware_interface: bp.BravoProtocol):
        super().__init__(parent_server)
        self.com = hardware_interface

    def get_CanReceiveContainer(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> List[bool]:
        occupied = self.com.inner_state.occupied
        can_receive = [not slot and not self.com.running_protocol for slot in occupied]
        return can_receive

    def get_CanGiveContainer(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> List[bool]:
        occupied = self.com.inner_state.occupied
        can_give = [slot and not self.com.running_protocol for slot in occupied]
        return can_give

    def ReceivedContainer(
        self, Site: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> ReceivedContainer_Responses:
        self.com.inner_state.occupied[Site] = True
        self.com.transfer_mode = False

    def ContainerGotTaken(
        self, Site: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> ContainerGotTaken_Responses:
        self.com.inner_state.occupied[Site] = False
        self.com.transfer_mode = False

    def TransportCancelled(
        self, Site: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> TransportCancelled_Responses:
        self.com.transfer_mode = False

    def PrepareToGiveContainerFromSite(
        self, Site: int, *, metadata: Dict[FullyQualifiedIdentifier, Any], instance: ObservableCommandInstance
    ) -> PrepareToGiveContainerFromSite_Responses:
        if not self.com.inner_state.occupied[Site]:
            return False
        self.com.transfer_mode = True
        instance.status = CommandExecutionStatus.running
        instance.estimated_remaining_time = timedelta(seconds=10)
        if self.com.finish_protocol(timeout=60):
            if self.com.get_head_out_of_way(Site):
                return True
        # timed out
        self.com.transfer_mode = False
        return False

    def PrepareToReceiveContainerOnSite(
        self, Site: int, *, metadata: Dict[FullyQualifiedIdentifier, Any], instance: ObservableCommandInstance
    ) -> PrepareToReceiveContainerOnSite_Responses:
        if self.com.inner_state.occupied[Site]:
            return False
        self.com.transfer_mode = True
        instance.status = CommandExecutionStatus.running
        instance.estimated_remaining_time = timedelta(seconds=10)
        if self.com.finish_protocol(timeout=60):
            if self.com.get_head_out_of_way(Site):
                return True
        # timed out
        self.com.transfer_mode = False
        return False
