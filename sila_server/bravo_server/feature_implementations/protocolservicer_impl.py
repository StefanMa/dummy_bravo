from __future__ import annotations

from typing import Any, Dict, List
import time
import logging

from sila2.framework import FullyQualifiedIdentifier, CommandExecutionStatus
from sila2.server import ObservableCommandInstance

from ..generated.protocolservicer import(
    ConnectToDevice_Responses,
    ProtocolServicerBase,
    RunProtocol_Responses,
    ProtocolNotFound,
    DeviceNotReachable,
    AbortProtocol_Responses,
)

import bravo_server.bravo_protocol as bp


class ProtocolServicerImpl(ProtocolServicerBase):
    def __init__(self, parent_server, hardware_interface: bp.BravoProtocol):
        self.com = hardware_interface
        super().__init__(parent_server)

    def get_AvailableProtocols(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> List[str]:
        return self.com.available_protocols()

    def AbortProtocol(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> AbortProtocol_Responses:
        self.com.abort_protocol()

    def ConnectToDevice(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> ConnectToDevice_Responses:
        answer = self.com.login()
        if not answer == bp.RetCode.SUCCESS:
            raise DeviceNotReachable("Login failed")
        answer = self.com.initialize()
        if answer == bp.RetCode.SUCCESS:
            return
        else:
            logging.error("Initialization Failed")
            raise DeviceNotReachable("Initialization failed.")

    def RunProtocol(
        self, Protocol: str, *, metadata: Dict[FullyQualifiedIdentifier, Any], instance: ObservableCommandInstance
    ) -> RunProtocol_Responses:
        if Protocol not in self.com.available_protocols():
            raise ProtocolNotFound()
        if self.com.running_protocol:
            logging.error("Device is already running protocol.")
            raise ProtocolNotFound("Device is already running some protocol.")
        instance.status = CommandExecutionStatus.waiting
        answer = self.com.run_protocol(Protocol)
        if answer == bp.RetCode.SUCCESS:
            instance.status = CommandExecutionStatus.running
            timeout = time.time() + 60
            while self.com.running_protocol:
                time.sleep(.05)
                if time.time() > timeout:
                    return
            logging.info("Protocol finished")
        else:
            raise ProtocolNotFound("device is not in the mood to run a protocol :-(")
