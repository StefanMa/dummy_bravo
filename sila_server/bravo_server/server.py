from sila2.server import SilaServer
from typing import Optional

from .feature_implementations.protocolservicer_impl import ProtocolServicerImpl
from .feature_implementations.robotinteractionservice_impl import RobotInteractionServiceImpl
from .generated.protocolservicer import ProtocolServicerFeature
from .generated.robotinteractionservice import RobotInteractionServiceFeature

from bravo_server.bravo_protocol import BravoProtocol


class Server(SilaServer):
    def __init__(self, simulation=True):
        super().__init__(
            server_name="AgilentBravoServer",
            server_type="AgilentBravoServer",
            server_version="0.1",
            server_description="A server for the agilent bravo pipetting robot. It runs using the VWorks4Lib.",
            server_vendor_url="https://gitlab.com/SiLA2/sila_python",
        )

        self.hardware_interface = BravoProtocol(simulation=simulation)

        self.protocolservicer = ProtocolServicerImpl(self, self.hardware_interface)
        self.robotinteractionservice = RobotInteractionServiceImpl(self, self.hardware_interface)

        self.set_feature_implementation(ProtocolServicerFeature, self.protocolservicer)
        self.set_feature_implementation(RobotInteractionServiceFeature, self.robotinteractionservice)

    def stop(self, **kwargs):
        self.hardware_interface.stop()
        super().stop(**kwargs)
