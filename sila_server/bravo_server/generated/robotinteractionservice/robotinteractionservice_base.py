# Generating by sila2.code_generator; sila2.__version__: 0.7.3
from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Dict, List

from sila2.framework import FullyQualifiedIdentifier
from sila2.server import FeatureImplementationBase, ObservableCommandInstance

from .robotinteractionservice_types import (
    ContainerGotTaken_Responses,
    PrepareToGiveContainerFromSite_Responses,
    PrepareToReceiveContainerOnSite_Responses,
    ReceivedContainer_Responses,
    TransportCancelled_Responses,
)


class RobotInteractionServiceBase(FeatureImplementationBase, ABC):
    @abstractmethod
    def get_CanReceiveContainer(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> List[bool]:
        """
        Enquiry a list of booleans indicating on which sites the device is currently able to receive a container.
            The list is sorted by site index.

        :param metadata: The SiLA Client Metadata attached to the call
        :return: Enquiry a list of booleans indicating on which sites the device is currently able to receive a container.
            The list is sorted by site index.
        """
        pass

    @abstractmethod
    def get_CanGiveContainer(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> List[bool]:
        """
        Enquiry a list of booleans indicating on which sites the device is currently able to provide a container.
            The list is sorted by site index.

        :param metadata: The SiLA Client Metadata attached to the call
        :return: Enquiry a list of booleans indicating on which sites the device is currently able to provide a container.
            The list is sorted by site index.
        """
        pass

    @abstractmethod
    def ReceivedContainer(
        self, Site: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> ReceivedContainer_Responses:
        """
        Notification, that a container was placed on the specified site.


        :param Site: The index of the site within the device.

        :param metadata: The SiLA Client Metadata attached to the call

        """
        pass

    @abstractmethod
    def ContainerGotTaken(
        self, Site: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> ContainerGotTaken_Responses:
        """
        Notification, that a container was taken from the specified site.


        :param Site: The index of the site within the device.

        :param metadata: The SiLA Client Metadata attached to the call

        """
        pass

    @abstractmethod
    def TransportCancelled(
        self, Site: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> TransportCancelled_Responses:
        """
        Notification that the Transport to/from the specified site got cancelled due to unforeseen errors.


        :param Site: The index of the site within the device.

        :param metadata: The SiLA Client Metadata attached to the call

        """
        pass

    @abstractmethod
    def PrepareToGiveContainerFromSite(
        self, Site: int, *, metadata: Dict[FullyQualifiedIdentifier, Any], instance: ObservableCommandInstance
    ) -> PrepareToGiveContainerFromSite_Responses:
        """
        Observable command telling a device to prepare for the robot taking a Container from the specified site.
            The observability models that this may take some time for some devices.


        :param Site: Index the the devices site that shall be accessed.

        :param metadata: The SiLA Client Metadata attached to the call
        :param instance: The command instance, enabling sending status updates to subscribed clients

        :return:

            - Ready: Boolean indicating whether the device is ready for access or can finally not be accessed.


        """
        pass

    @abstractmethod
    def PrepareToReceiveContainerOnSite(
        self, Site: int, *, metadata: Dict[FullyQualifiedIdentifier, Any], instance: ObservableCommandInstance
    ) -> PrepareToReceiveContainerOnSite_Responses:
        """
        Observable command telling a device to prepare for the robot placing a Container on the specified site.
            The observability models that this may take some time for some devices.


        :param Site: Index the the devices site that shall be accessed.

        :param metadata: The SiLA Client Metadata attached to the call
        :param instance: The command instance, enabling sending status updates to subscribed clients

        :return:

            - Ready: Boolean indicating whether the device is ready for access or can finally not be accessed.


        """
        pass
