# Generating by sila2.code_generator; sila2.__version__: 0.7.3
from __future__ import annotations

from typing import NamedTuple


class ReceivedContainer_Responses(NamedTuple):

    pass


class ContainerGotTaken_Responses(NamedTuple):

    pass


class TransportCancelled_Responses(NamedTuple):

    pass


class PrepareToGiveContainerFromSite_Responses(NamedTuple):

    Ready: bool
    """
    Boolean indicating whether the device is ready for access or can finally not be accessed.
    """


class PrepareToReceiveContainerOnSite_Responses(NamedTuple):

    Ready: bool
    """
    Boolean indicating whether the device is ready for access or can finally not be accessed.
    """
