from __future__ import annotations

from typing import Iterable, List, Optional

from robotinteractionservice_types import (
    ContainerGotTaken_Responses,
    PrepareToGiveContainerFromSite_Responses,
    PrepareToReceiveContainerOnSite_Responses,
    ReceivedContainer_Responses,
    TransportCancelled_Responses,
)
from sila2.client import ClientMetadataInstance, ClientObservableCommandInstance, ClientUnobservableProperty

class RobotInteractionServiceClient:
    """

    This service is designed to enable communication between a robot that transports containers and any device these
    containers are transported to. The latter should have this feature. The methods get called by the transporting
    robot. The methods include an enquiry whether a transport to/from which site is possible, an
    observable command to prepare access and a notification, that the transport was successful. The latter can be
    left empty in case the device has an own sensor for it. Finally there is a notification that the transport got
    cancelled. The communication should be:
    1. The robot asks all (usually two) participating devices whether transport is possible.
    2. If so, the robot tells all devices to prepare access and waits with each access for the corresponding
    positive reply.
    3. After each access, the robot senses to be successful the corresponding device is notified. If the transport
    failed (f.e. container was dropped) all waiting devices should receive cancel notifications.

    """

    CanReceiveContainer: ClientUnobservableProperty[List[bool]]
    """
    
            Enquiry a list of booleans indicating on which sites the device is currently able to receive a container.
            The list is sorted by site index.
        
    """

    CanGiveContainer: ClientUnobservableProperty[List[bool]]
    """
    
            Enquiry a list of booleans indicating on which sites the device is currently able to provide a container.
            The list is sorted by site index.
        
    """
    def ReceivedContainer(
        self, Site: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ReceivedContainer_Responses:
        """
        Notification, that a container was placed on the specified site.
        """
        ...
    def ContainerGotTaken(
        self, Site: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ContainerGotTaken_Responses:
        """
        Notification, that a container was taken from the specified site.
        """
        ...
    def TransportCancelled(
        self, Site: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> TransportCancelled_Responses:
        """

        Notification that the Transport to/from the specified site got cancelled due to unforeseen errors.

        """
        ...
    def PrepareToGiveContainerFromSite(
        self, Site: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ClientObservableCommandInstance[None, PrepareToGiveContainerFromSite_Responses]:
        """

        Observable command telling a device to prepare for the robot taking a Container from the specified site.
        The observability models that this may take some time for some devices.

        """
        ...
    def PrepareToReceiveContainerOnSite(
        self, Site: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ClientObservableCommandInstance[None, PrepareToReceiveContainerOnSite_Responses]:
        """

        Observable command telling a device to prepare for the robot placing a Container on the specified site.
        The observability models that this may take some time for some devices.

        """
        ...
