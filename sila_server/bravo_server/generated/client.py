# Generating by sila2.code_generator; sila2.__version__: 0.7.3
from __future__ import annotations

from typing import Set

from sila2.client import SilaClient
from sila2.framework import FullyQualifiedIdentifier

from .protocolservicer import DeviceNotReachable, ProtocolNotFound, ProtocolServicerClient, ProtocolServicerFeature
from .robotinteractionservice import RobotInteractionServiceClient


class Client(SilaClient):

    ProtocolServicer: ProtocolServicerClient

    RobotInteractionService: RobotInteractionServiceClient

    _expected_features: Set[FullyQualifiedIdentifier] = {
        FullyQualifiedIdentifier("org.silastandard/core/SiLAService/v1"),
        FullyQualifiedIdentifier("de.tuberlin.bioprocess/storing/ProtocolServicer/v1"),
        FullyQualifiedIdentifier("de.tuberlin.bioprocess/storing/RobotInteractionService/v1"),
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._register_defined_execution_error_class(
            ProtocolServicerFeature.defined_execution_errors["ProtocolNotFound"], ProtocolNotFound
        )

        self._register_defined_execution_error_class(
            ProtocolServicerFeature.defined_execution_errors["DeviceNotReachable"], DeviceNotReachable
        )
