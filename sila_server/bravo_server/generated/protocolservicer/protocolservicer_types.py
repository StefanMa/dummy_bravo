# Generating by sila2.code_generator; sila2.__version__: 0.7.3
from __future__ import annotations

from typing import NamedTuple


class AbortProtocol_Responses(NamedTuple):

    pass


class ConnectToDevice_Responses(NamedTuple):

    pass


class RunProtocol_Responses(NamedTuple):

    pass
