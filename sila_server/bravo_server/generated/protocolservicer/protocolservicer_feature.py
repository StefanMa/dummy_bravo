# Generating by sila2.code_generator; sila2.__version__: 0.7.3
from os.path import dirname, join

from sila2.framework import Feature

ProtocolServicerFeature = Feature(open(join(dirname(__file__), "ProtocolServicer.sila.xml")).read())
