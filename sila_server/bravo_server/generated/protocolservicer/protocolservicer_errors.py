# Generating by sila2.code_generator; sila2.__version__: 0.7.3
from __future__ import annotations

from typing import Optional

from sila2.framework.errors.defined_execution_error import DefinedExecutionError

from .protocolservicer_feature import ProtocolServicerFeature


class ProtocolNotFound(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = "The given protocol was not found in the protocols directory"
        super().__init__(ProtocolServicerFeature.defined_execution_errors["ProtocolNotFound"], message=message)


class DeviceNotReachable(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = "Loading of device file or connection to the device failed."
        super().__init__(ProtocolServicerFeature.defined_execution_errors["DeviceNotReachable"], message=message)
