# Generating by sila2.code_generator; sila2.__version__: 0.7.3
# -----
# This class does not do anything useful at runtime. Its only purpose is to provide type annotations.
# Since sphinx does not support .pyi files (yet?), so this is a .py file.
# -----

from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:

    from typing import Iterable, List, Optional

    from protocolservicer_types import AbortProtocol_Responses, ConnectToDevice_Responses, RunProtocol_Responses
    from sila2.client import ClientMetadataInstance, ClientObservableCommandInstance, ClientUnobservableProperty


class ProtocolServicerClient:
    """
    SiLA feature for interfacing the VWorks API. It is not complete and specialized for the agilent bravo.
    """

    AvailableProtocols: ClientUnobservableProperty[List[str]]
    """
    List of available protocols
    """

    def AbortProtocol(self, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None) -> AbortProtocol_Responses:
        """
        Aborts the currently running protocol. Does nothing if no protocol is running
        """
        ...

    def ConnectToDevice(
        self, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ConnectToDevice_Responses:
        """
        Connects the server to the Agilent-Bravo.
        """
        ...

    def RunProtocol(
        self, Protocol: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ClientObservableCommandInstance[RunProtocol_Responses]:
        """
        Run Protocol
        """
        ...
