# Generating by sila2.code_generator; sila2.__version__: 0.7.3
from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Dict, List

from sila2.framework import FullyQualifiedIdentifier
from sila2.server import FeatureImplementationBase, ObservableCommandInstance

from .protocolservicer_types import AbortProtocol_Responses, ConnectToDevice_Responses, RunProtocol_Responses


class ProtocolServicerBase(FeatureImplementationBase, ABC):
    @abstractmethod
    def get_AvailableProtocols(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> List[str]:
        """
        List of available protocols

        :param metadata: The SiLA Client Metadata attached to the call
        :return: List of available protocols
        """
        pass

    @abstractmethod
    def AbortProtocol(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> AbortProtocol_Responses:
        """
        Aborts the currently running protocol. Does nothing if no protocol is running


        :param metadata: The SiLA Client Metadata attached to the call

        """
        pass

    @abstractmethod
    def ConnectToDevice(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> ConnectToDevice_Responses:
        """
        Connects the server to the Agilent-Bravo.


        :param metadata: The SiLA Client Metadata attached to the call

        """
        pass

    @abstractmethod
    def RunProtocol(
        self, Protocol: str, *, metadata: Dict[FullyQualifiedIdentifier, Any], instance: ObservableCommandInstance
    ) -> RunProtocol_Responses:
        """
        Run Protocol


        :param Protocol: Name of the protocol(with or without '.pro')

        :param metadata: The SiLA Client Metadata attached to the call
        :param instance: The command instance, enabling sending status updates to subscribed clients

        """
        pass
