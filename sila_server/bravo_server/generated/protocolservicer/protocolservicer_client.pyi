from __future__ import annotations

from typing import Iterable, List, Optional

from protocolservicer_types import ConnectToDevice_Responses, RunProtocol_Responses
from sila2.client import ClientMetadataInstance, ClientObservableCommandInstance, ClientUnobservableProperty

class ProtocolServicerClient:
    """

    SiLA feature for interfacing the VWorks API. It is not complete and specialized for the agilent bravo.

    """

    AvailableProtocols: ClientUnobservableProperty[List[str]]
    """
    List of available protocols
    """
    def ConnectToDevice(
        self, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ConnectToDevice_Responses:
        """
        Connects the server to the Agilent-Bravo.
        """
        ...
    def RunProtocol(
        self, Protocol: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ClientObservableCommandInstance[None, RunProtocol_Responses]:
        """
        Run Protocol
        """
        ...
