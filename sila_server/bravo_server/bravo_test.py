"""
A unittest for the cytomat client-server pair. It uses the virtual_cytomat to emulate a real one.
The test can be run in fast-mode for fast testing or in real mode for realistic reaction/working times of the device.
"""
import logging
import time
import unittest
import os
from threading import Thread
from lara_simulation.deviceTest import DeviceTest
from bravo_server.server import Server as BravoServer
from bravo_server.generated.client import Client as BravoClient


class BravoTest(DeviceTest, unittest.TestCase):
    def __init__(self, server_ip='127.0.0.1', port=50051, simulation=True):
        DeviceTest.__init__(self)
        unittest.TestCase.__init__(self)
        self.server = BravoServer(simulation)
        self.server.start_insecure(server_ip, port, enable_discovery=True)
        self.client = BravoClient(server_ip, port)

    def is_error(self, feedback) -> bool:
        # evaluate whether the feedback is an error
        return False

    def manual_adjustments(self):
        print("Server running. Press Ctrl+C to stop")

    def test_process(self):
        self.server.hardware_interface.inner_state.reset()
        self.server.hardware_interface.inner_state.protocol_dir = "."
        interaction = self.client.RobotInteractionService
        protocol = self.client.ProtocolServicer

        # init
        protocol.ConnectToDevice()

        # load
        site = 1
        assert interaction.CanReceiveContainer.get()[site]
        self.finish_obs_cmd(interaction.PrepareToReceiveContainerOnSite(site))
        interaction.ReceivedContainer(site)
        # run protocol
        file = 'Head_Left.pro'
        assert file in protocol.AvailableProtocols.get()
        self.finish_obs_cmd(protocol.RunProtocol(file))

        # unload
        assert interaction.CanGiveContainer.get()[site]
        self.finish_obs_cmd(interaction.PrepareToGiveContainerFromSite(site))
        interaction.ContainerGotTaken(site)


if __name__ == '__main__':
    mytest = BravoTest()
    mytest.runSimulation()

